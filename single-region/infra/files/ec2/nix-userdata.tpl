#!/bin/bash -v

sudo su -

export NEWHOSTNAME="${hostname}"

hostname $NEWHOSTNAME
echo "$NEWHOSTNAME" > /etc/hostname
echo "127.0.0.1 $NEWHOSTNAME" > /etc/hosts
unset NEWHOSTNAME

systemctl restart systemd-logind.service
