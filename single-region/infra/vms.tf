locals {
  nodes = [
    for node in aws_instance.vm.* :
    {
      instance   = index(aws_instance.vm.*, node)
      public_ip  = node.public_ip
      private_ip = node.private_ip
    }
  ]

  # inventory = {
  #     all = {
  #         children = [
  #             for node in aws_instance.vm.*:
  #             {
  #                 index(aws_instance.vm.*, node) = {
  #                     hosts = {
  #                         node.public_ip
  #                     }
  #                 }
  #             }
  #         ]
  #     }
  # }

}
resource "aws_security_group" "vm" {
  name        = "${var.project_name}-vm-sg"
  description = "Controls in/out traffic for vm networking"
  vpc_id      = aws_vpc.vpc.id

  tags = merge(
    local.common_tags, {
      "Name" = "${var.project_name}-vm-sg"
  })
}

resource "aws_security_group_rule" "inbound_internal_vm" {
  security_group_id = aws_security_group.vm.id
  description       = "Allow all traffic within vm"
  type              = "ingress"
  from_port         = "0"
  to_port           = "65535"
  protocol          = "all"
  self              = "true"
}

resource "aws_security_group_rule" "inbound_ssh_to_vm" {
  security_group_id = aws_security_group.vm.id
  description       = "Allow ssh protocol for any"
  type              = "ingress"
  from_port         = "22"
  to_port           = "22"
  protocol          = "tcp"
  cidr_blocks       = ["0.0.0.0/0"]
}


resource "aws_security_group_rule" "vm_outbound_any" {
  security_group_id = aws_security_group.vm.id
  type              = "egress"
  from_port         = 0
  to_port           = 0
  protocol          = "all"
  cidr_blocks       = ["0.0.0.0/0"]
}


resource "aws_network_interface" "vm" {
  count = length(var.vm)

  subnet_id       = aws_subnet.pb_sn_az[count.index].id
  private_ips     = [cidrhost(cidrsubnet(aws_vpc.vpc.cidr_block, 6, count.index), 6 + count.index)]
  security_groups = [aws_security_group.vm.id]

  tags = merge(
    local.common_tags, {
      "Name" = "${var.project_name}-vm-${var.aws_az[count.index]}-eni"
  })
}

resource "aws_instance" "vm" {
  count = length(var.vm)

  ami               = var.vm[count.index].ami_id
  instance_type     = var.vm[count.index].instance_type
  availability_zone = "${var.aws_region}${var.aws_az[count.index]}"
  key_name          = aws_key_pair.ec2_keypair.key_name
  user_data = templatefile("./files/ec2/nix-userdata.tpl", {
    hostname = "${var.project_name}-vm-${var.aws_az[count.index]}"
  })
  disable_api_termination = false

  network_interface {
    device_index         = 0
    network_interface_id = aws_network_interface.vm[count.index].id
  }

  root_block_device {
      # volume_type           = "io1"
      volume_size           = "100"
      delete_on_termination = true
  }

  lifecycle {
    #[TEMP] Due to user-data forces new resource when you move terraform apply from windows to linux and vice versa
    ignore_changes = [user_data]
  }

  tags = merge(
    local.common_tags, {
      "Name" = "${var.project_name}-vm-${var.aws_az[count.index]}"
  })

  # volume_tags = merge(
  #     local.common_tags, {
  #         "Name"   = "${var.project_name}-vm-${var.aws_az[count.index]}-ebs"
  # })
}

resource "aws_network_interface" "cm" {
  subnet_id       = aws_subnet.pb_sn_cm_az.id
  private_ips     = [cidrhost(cidrsubnet(aws_vpc.vpc.cidr_block, 6, 9), 15)]
  security_groups = [aws_security_group.vm.id]

  tags = merge(
    local.common_tags, {
      "Name" = "${var.project_name}-cm-a-eni"
  })
}

resource "aws_instance" "cm" {
  ami               = var.cm.ami_id
  instance_type     = var.cm.instance_type
  availability_zone = "${var.aws_region}a"
  key_name          = aws_key_pair.ec2_keypair.key_name
  user_data = templatefile("./files/ec2/nix-userdata.tpl", {
    hostname = "${var.project_name}-cm-a"
  })
  disable_api_termination = false

  network_interface {
    device_index         = 0
    network_interface_id = aws_network_interface.cm.id
  }

  # root_block_device {
  #     volume_type           = var.cm.root_block_device.volume_type
  #     volume_size           = var.cm.root_block_device.volume_size
  #     delete_on_termination = var.cm.root_block_device.is_delete_on_termination
  #     encrypted             = var.cm.root_block_device.is_encrypted
  # }

  lifecycle {
    #[TEMP] Due to user-data forces new resource when you move terraform apply from windows to linux and vice versa
    ignore_changes = [user_data]
  }

  tags = merge(
    local.common_tags, {
      "Name" = "${var.project_name}-cm-a"
  })

  # volume_tags = merge(
  #     local.common_tags, {
  #         "Name"   = "${var.project_name}-vm-${var.aws_az[count.index]}-ebs"
  # })
}

resource "local_file" "host" {
  content = templatefile("files/templates/hosts", {
    node_1 = local.nodes[0].public_ip
    node_2 = local.nodes[1].public_ip
    node_3 = local.nodes[2].public_ip
    # node_4 = local.nodes[3].public_ip
    # node_5 = local.nodes[4].public_ip
    # node_6 = local.nodes[5].public_ip
    # node_7 = local.nodes[6].public_ip
    cm     = aws_instance.cm.public_ip
  })
  filename = "../os/inventory/hosts"
}

resource "local_file" "node_1" {
  content = templatefile("files/templates/node.yaml", {
    cluster_name   = "Amazon cassandra cluster"
    seeds          = join(",", local.nodes.*.private_ip)
    listen_address = local.nodes[0].private_ip
    private_ip     = local.nodes[0].private_ip
  })
  filename = "../os/inventory/group_vars/node-1/conf.yaml"
}

resource "local_file" "node_2" {
  content = templatefile("files/templates/node.yaml", {
    cluster_name   = "Amazon cassandra cluster"
    seeds          = join(",", local.nodes.*.private_ip)
    listen_address = local.nodes[1].private_ip
    private_ip     = local.nodes[1].private_ip
  })
  filename = "../os/inventory/group_vars/node-2/conf.yaml"
}

resource "local_file" "node_3" {
  content = templatefile("files/templates/node.yaml", {
    cluster_name   = "Amazon cassandra cluster"
    seeds          = join(",", local.nodes.*.private_ip)
    listen_address = local.nodes[2].private_ip
    private_ip     = local.nodes[2].private_ip
  })
  filename = "../os/inventory/group_vars/node-3/conf.yaml"
}

# resource "local_file" "node_4" {
#   content = templatefile("files/templates/node.yaml", {
#     cluster_name   = "Amazon cassandra cluster"
#     seeds          = join(",", local.nodes.*.private_ip)
#     listen_address = local.nodes[3].private_ip
#     private_ip     = local.nodes[3].private_ip
#   })
#   filename = "../os/inventory/group_vars/node-4/conf.yaml"
# }

# resource "local_file" "node_5" {
#   content = templatefile("files/templates/node.yaml", {
#     cluster_name   = "Amazon cassandra cluster"
#     seeds          = join(",", local.nodes.*.private_ip)
#     listen_address = local.nodes[4].private_ip
#     private_ip     = local.nodes[4].private_ip
#   })
#   filename = "../os/inventory/group_vars/node-5/conf.yaml"
# }

# resource "local_file" "node_6" {
#   content = templatefile("files/templates/node.yaml", {
#     cluster_name   = "Amazon cassandra cluster"
#     seeds          = join(",", local.nodes.*.private_ip)
#     listen_address = local.nodes[5].private_ip
#     private_ip     = local.nodes[5].private_ip
#   })
#   filename = "../os/inventory/group_vars/node-6/conf.yaml"
# }

# resource "local_file" "node_7" {
#   content = templatefile("files/templates/node.yaml", {
#     cluster_name   = "Amazon cassandra cluster"
#     seeds          = join(",", local.nodes.*.private_ip)
#     listen_address = local.nodes[6].private_ip
#     private_ip     = local.nodes[6].private_ip
#   })
#   filename = "../os/inventory/group_vars/node-7/conf.yaml"
# }

#DATADOG installation 
# DD_AGENT_MAJOR_VERSION=7 DD_API_KEY=71dfcc201ba04a262df0d01ea5de7065 DD_SITE="datadoghq.eu" bash -c "$(curl -L https://s3.amazonaws.com/dd-agent/scripts/install_script.sh)"
# cd /etc/datadog-agent/conf.d/cassandra.d/
# sudo mv conf.yaml.example conf.yaml
# cd ../cassandra_nodetool.d/
# sudo mv conf.yaml.example conf.yaml
# sudo service datadog-agent restart
# sudo datadog-agent status