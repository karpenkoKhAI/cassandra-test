#Create Amazon VPC with specific CIDR block
resource "aws_vpc" "vpc" {
  cidr_block           = var.vpc_cidr
  enable_dns_hostnames = true

  tags = merge(
    local.common_tags, {
      "Name" = "${var.project_name}-vpc"
  })
}

#Create Amazon DHCP
resource "aws_vpc_dhcp_options" "dhcp" {
  domain_name_servers = ["AmazonProvidedDNS"]

  tags = merge(
    local.common_tags, {
      "Name" = "${var.project_name}-dhcp"
  })
}

#Associate Amazon DHCP and Amazon VPC
resource "aws_vpc_dhcp_options_association" "vpc_dhcp" {
  vpc_id          = aws_vpc.vpc.id
  dhcp_options_id = aws_vpc_dhcp_options.dhcp.id
}

#Create Amazon IGW
resource "aws_internet_gateway" "igw" {
  vpc_id = aws_vpc.vpc.id

  tags = merge(
    local.common_tags, {
      "Name" = "${var.project_name}-igw"
  })
}

#Create `main` Amazon Route Table
resource "aws_route_table" "main" {
  vpc_id = aws_vpc.vpc.id

  tags = merge(
    local.common_tags, {
      "Name" = "${var.project_name}-rt"
  })
}

#Create default route table rule
resource "aws_route" "default" {
  route_table_id         = aws_route_table.main.id
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = aws_internet_gateway.igw.id
}

#Create Amazon EC2 key pair
resource "aws_key_pair" "ec2_keypair" {
  key_name   = "${var.project_name}-pub-key"
  public_key = var.ec2_keypair_public_key
}


#Create public subnet
resource "aws_subnet" "pb_sn_az" {
  count = length(var.aws_az)

  vpc_id            = aws_vpc.vpc.id
  cidr_block        = cidrsubnet(aws_vpc.vpc.cidr_block, 6, count.index)
  availability_zone = "${var.aws_region}${var.aws_az[count.index]}"

  map_public_ip_on_launch = true

  tags = merge(
    local.common_tags, {
      "Name" = "${var.project_name}-pb-sn-${var.aws_az[count.index]}"
  })
}

#Associate public subnet with main route table
resource "aws_route_table_association" "pb_sn_az" {
  count = length(var.aws_az)

  subnet_id      = aws_subnet.pb_sn_az[count.index].id
  route_table_id = aws_route_table.main.id
}


#Create public subnet
resource "aws_subnet" "pb_sn_cm_az" {
  vpc_id            = aws_vpc.vpc.id
  cidr_block        = cidrsubnet(aws_vpc.vpc.cidr_block, 6, 9)
  availability_zone = "${var.aws_region}a"

  map_public_ip_on_launch = true

  tags = merge(
    local.common_tags, {
      "Name" = "${var.project_name}-pb-sn-cm-a"
  })
}

#Associate public subnet with main route table
resource "aws_route_table_association" "pb_sn_cm_az" {
  subnet_id      = aws_subnet.pb_sn_cm_az.id
  route_table_id = aws_route_table.main.id
}