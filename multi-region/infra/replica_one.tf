#London
locals {
  replica_one = {
    region        = "eu-west-2"
    vpc_cidr      = "10.161.0.0/16"
    ami_id        = "ami-0e2970aea15ce4984"
    instance_type = "t2.xlarge"
  }
}

#Create Amazon VPC with specific CIDR block
resource "aws_vpc" "replica_one_vpc" {
  provider = aws.replica_one

  cidr_block           = local.replica_one.vpc_cidr
  enable_dns_hostnames = true

  tags = merge(
    local.common_tags, {
      "Name" = "${var.project_name}-${local.replica_one.region}-${local.replica_one.region}-${local.replica_one.region}-vpc"
  })
}

#Create Amazon DHCP
resource "aws_vpc_dhcp_options" "replica_one_dhcp" {
  provider = aws.replica_one

  domain_name_servers = ["AmazonProvidedDNS"]

  tags = merge(
    local.common_tags, {
      "Name" = "${var.project_name}-${local.replica_one.region}-${local.replica_one.region}-${local.replica_one.region}-dhcp"
  })
}

#Associate Amazon DHCP and Amazon VPC
resource "aws_vpc_dhcp_options_association" "replica_one_vpc_dhcp" {
  provider = aws.replica_one

  vpc_id          = aws_vpc.replica_one_vpc.id
  dhcp_options_id = aws_vpc_dhcp_options.replica_one_dhcp.id
}

#Create Amazon IGW
resource "aws_internet_gateway" "replica_one_igw" {
  provider = aws.replica_one

  vpc_id = aws_vpc.replica_one_vpc.id

  tags = merge(
    local.common_tags, {
      "Name" = "${var.project_name}-${local.replica_one.region}-${local.replica_one.region}-${local.replica_one.region}-igw"
  })
}

#Create `main` Amazon Route Table
resource "aws_route_table" "replica_one_main" {
  provider = aws.replica_one

  vpc_id = aws_vpc.replica_one_vpc.id

  tags = merge(
    local.common_tags, {
      "Name" = "${var.project_name}-${local.replica_one.region}-${local.replica_one.region}-${local.replica_one.region}-rt"
  })
}

#Create default route table rule
resource "aws_route" "replica_one_default" {
  provider = aws.replica_one

  route_table_id         = aws_route_table.replica_one_main.id
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = aws_internet_gateway.replica_one_igw.id
}

resource "aws_route" "replica_one_to_replica_two" {
  provider = aws.replica_one

  route_table_id            = aws_route_table.replica_one_main.id
  destination_cidr_block    = local.replica_two.vpc_cidr
  vpc_peering_connection_id = aws_vpc_peering_connection.replica_one_to_replica_two.id
}

resource "aws_route" "replica_one_to_replica_three" {
  provider = aws.replica_one

  route_table_id            = aws_route_table.replica_one_main.id
  destination_cidr_block    = local.replica_three.vpc_cidr
  vpc_peering_connection_id = aws_vpc_peering_connection.replica_one_to_replica_three.id
}

resource "aws_route" "replica_one_to_client_one" {
  provider = aws.replica_one

  route_table_id            = aws_route_table.replica_one_main.id
  destination_cidr_block    = local.client_one.vpc_cidr
  vpc_peering_connection_id = aws_vpc_peering_connection.client_one_to_replica_one.id
}

resource "aws_route" "replica_one_to_client_two" {
  provider = aws.replica_one

  route_table_id            = aws_route_table.replica_one_main.id
  destination_cidr_block    = local.client_two.vpc_cidr
  vpc_peering_connection_id = aws_vpc_peering_connection.client_two_to_replica_one.id
}

#Create Amazon EC2 key pair
resource "aws_key_pair" "replica_one_ec2_keypair" {
  provider = aws.replica_one

  key_name   = "${var.project_name}-${local.replica_one.region}-${local.replica_one.region}-${local.replica_one.region}-pub-key"
  public_key = var.ec2_keypair_public_key
}


#Create public subnet
resource "aws_subnet" "replica_one_pb_sn_az" {
  provider = aws.replica_one

  vpc_id            = aws_vpc.replica_one_vpc.id
  cidr_block        = cidrsubnet(aws_vpc.replica_one_vpc.cidr_block,8, 1)
  availability_zone = "${local.replica_one.region}a"

  map_public_ip_on_launch = true

  tags = merge(
    local.common_tags, {
      "Name" = "${var.project_name}-${local.replica_one.region}-${local.replica_one.region}-${local.replica_one.region}-pb-sn-a"
  })
}

#Associate public subnet with main route table
resource "aws_route_table_association" "replica_one_pb_sn_az" {
  provider = aws.replica_one

  subnet_id      = aws_subnet.replica_one_pb_sn_az.id
  route_table_id = aws_route_table.replica_one_main.id
}

#Configure VPC peering to replica_two
resource "aws_vpc_peering_connection" "replica_one_to_replica_two" {
  provider = aws.replica_one

  vpc_id      = aws_vpc.replica_one_vpc.id
  peer_vpc_id = aws_vpc.replica_two_vpc.id
  peer_region = local.replica_two.region
  auto_accept = false
  
  tags = merge(
    local.common_tags,
    {
      "Name" = "one-to-two-peer"
    }
  )
}

resource "aws_vpc_peering_connection_accepter" "replica_one_to_replica_two" {
  provider = aws.replica_two

  vpc_peering_connection_id = aws_vpc_peering_connection.replica_one_to_replica_two.id
  auto_accept               = true

  tags = merge(
    local.common_tags,
    {
      "Name" = "one-to-two-peer"
    }
  )
}

#Configure VPC peering to replica_three
resource "aws_vpc_peering_connection" "replica_one_to_replica_three" {
  provider    = aws.replica_one
  vpc_id      = aws_vpc.replica_one_vpc.id
  peer_vpc_id = aws_vpc.replica_three_vpc.id
  peer_region = local.replica_three.region
  auto_accept = false

  tags = merge(
    local.common_tags,
    {
      "Name" = "one-to-three-peer"
    }
  )
}

resource "aws_vpc_peering_connection_accepter" "replica_one_to_replica_three" {
  provider = aws.replica_three

  vpc_peering_connection_id = aws_vpc_peering_connection.replica_one_to_replica_three.id
  auto_accept               = true

  tags = merge(
    local.common_tags,
    {
      "Name" = "one-to-three-peer"
    }
  )
}

resource "aws_security_group" "replica_one" {
  provider    = aws.replica_one
  name   = "${var.project_name}-${local.replica_one.region}-${local.replica_one.region}-vm-sg"
  vpc_id = aws_vpc.replica_one_vpc.id

  tags = merge(
    local.common_tags, {
      "Name" = "${var.project_name}-${local.replica_one.region}-${local.replica_one.region}-vm-sg"
  })
}

resource "aws_security_group_rule" "inbound_cluster_to_replica_one_vm" {
  provider    = aws.replica_one
  security_group_id = aws_security_group.replica_one.id
  description       = "Allow ssh protocol for any"
  type              = "ingress"
  from_port         = "0"
  to_port           = "65535"
  protocol          = "tcp"
  cidr_blocks = [
    "0.0.0.0/0"
  ]
}

resource "aws_security_group_rule" "replica_one_vm_outbound_any" {
  provider    = aws.replica_one
  security_group_id = aws_security_group.replica_one.id
  type              = "egress"
  from_port         = 0
  to_port           = 0
  protocol          = "all"
  cidr_blocks       = ["0.0.0.0/0"]
}


resource "aws_network_interface" "replica_one_vm" {
  provider = aws.replica_one

  subnet_id       = aws_subnet.replica_one_pb_sn_az.id
  private_ips     = [cidrhost(cidrsubnet(aws_vpc.replica_one_vpc.cidr_block,8, 1), 5)]
  security_groups = [aws_security_group.replica_one.id]

  tags = merge(
    local.common_tags, {
      "Name" = "${var.project_name}-${local.replica_one.region}-vm-a-eni"
  })
}

