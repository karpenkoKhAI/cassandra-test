#London
locals {
  client_two = {
    region        = "us-west-2"
    vpc_cidr      = "10.159.0.0/16"
    ami_id        = "ami-08092cd60015a48a5"
    instance_type = "t2.xlarge"
  }
}

#Create Amazon VPC with specific CIDR block
resource "aws_vpc" "client_two_vpc" {
  provider = aws.client_two

  cidr_block           = local.client_two.vpc_cidr
  enable_dns_hostnames = true

  tags = merge(
    local.common_tags, {
      "Name" = "${var.project_name}-${local.client_two.region}-${local.client_two.region}-${local.client_two.region}-vpc"
  })
}

#Create Amazon DHCP
resource "aws_vpc_dhcp_options" "client_two_dhcp" {
  provider = aws.client_two

  domain_name_servers = ["AmazonProvidedDNS"]

  tags = merge(
    local.common_tags, {
      "Name" = "${var.project_name}-${local.client_two.region}-${local.client_two.region}-${local.client_two.region}-dhcp"
  })
}

#Associate Amazon DHCP and Amazon VPC
resource "aws_vpc_dhcp_options_association" "client_two_vpc_dhcp" {
  provider = aws.client_two

  vpc_id          = aws_vpc.client_two_vpc.id
  dhcp_options_id = aws_vpc_dhcp_options.client_two_dhcp.id
}

#Create Amazon IGW
resource "aws_internet_gateway" "client_two_igw" {
  provider = aws.client_two

  vpc_id = aws_vpc.client_two_vpc.id

  tags = merge(
    local.common_tags, {
      "Name" = "${var.project_name}-${local.client_two.region}-${local.client_two.region}-${local.client_two.region}-igw"
  })
}

#Create `main` Amazon Route Table
resource "aws_route_table" "client_two_main" {
  provider = aws.client_two

  vpc_id = aws_vpc.client_two_vpc.id

  tags = merge(
    local.common_tags, {
      "Name" = "${var.project_name}-${local.client_two.region}-${local.client_two.region}-${local.client_two.region}-rt"
  })
}

#Create default route table rule
resource "aws_route" "client_two_default" {
  provider = aws.client_two

  route_table_id         = aws_route_table.client_two_main.id
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = aws_internet_gateway.client_two_igw.id
}

resource "aws_route" "client_two_to_replica_one" {
  provider = aws.client_two

  route_table_id            = aws_route_table.client_two_main.id
  destination_cidr_block    = local.replica_one.vpc_cidr
  vpc_peering_connection_id = aws_vpc_peering_connection.client_two_to_replica_one.id
}

#Create Amazon EC2 key pair
resource "aws_key_pair" "client_two_ec2_keypair" {
  provider = aws.client_two

  key_name   = "${var.project_name}-${local.client_two.region}-${local.client_two.region}-${local.client_two.region}-pub-key"
  public_key = var.ec2_keypair_public_key
}


#Create public subnet
resource "aws_subnet" "client_two_pb_sn_az" {
  provider = aws.client_two

  vpc_id            = aws_vpc.client_two_vpc.id
  cidr_block        = cidrsubnet(aws_vpc.client_two_vpc.cidr_block,8, 1)
  availability_zone = "${local.client_two.region}a"

  map_public_ip_on_launch = true

  tags = merge(
    local.common_tags, {
      "Name" = "${var.project_name}-${local.client_two.region}-${local.client_two.region}-${local.client_two.region}-pb-sn-a"
  })
}

#Associate public subnet with main route table
resource "aws_route_table_association" "client_two_pb_sn_az" {
  provider = aws.client_two

  subnet_id      = aws_subnet.client_two_pb_sn_az.id
  route_table_id = aws_route_table.client_two_main.id
}

#Configure VPC peering to replica_two
resource "aws_vpc_peering_connection" "client_two_to_replica_one" {
  provider = aws.client_two

  vpc_id      = aws_vpc.client_two_vpc.id
  peer_vpc_id = aws_vpc.replica_one_vpc.id
  peer_region = local.replica_one.region
  auto_accept = false

  tags = merge(
    local.common_tags,
    {
      "Name" = "client-to-one-peer"
    }
  )
}

resource "aws_vpc_peering_connection_accepter" "client_two_to_replica_one" {
  provider = aws.replica_one

  vpc_peering_connection_id = aws_vpc_peering_connection.client_two_to_replica_one.id
  auto_accept               = true

  tags = merge(
    local.common_tags,
    {
      "Name" = "client-to-one-peer"
    }
  )
}

resource "aws_security_group" "client_two" {
  provider    = aws.client_two
  name   = "${var.project_name}-${local.client_two.region}-${local.client_two.region}-vm-sg"
  vpc_id = aws_vpc.client_two_vpc.id

  tags = merge(
    local.common_tags, {
      "Name" = "${var.project_name}-${local.client_two.region}-${local.client_two.region}-vm-sg"
  })
}

resource "aws_security_group_rule" "inbound_cluster_to_client_two_vm" {
  provider    = aws.client_two
  security_group_id = aws_security_group.client_two.id
  description       = "Allow ssh protocol for any"
  type              = "ingress"
  from_port         = "0"
  to_port           = "65535"
  protocol          = "tcp"
  cidr_blocks = [
    "0.0.0.0/0"
  ]
}

resource "aws_security_group_rule" "client_two_vm_outbound_any" {
  provider    = aws.client_two
  security_group_id = aws_security_group.client_two.id
  type              = "egress"
  from_port         = 0
  to_port           = 0
  protocol          = "all"
  cidr_blocks       = ["0.0.0.0/0"]
}


resource "aws_network_interface" "client_two_vm" {
  provider = aws.client_two

  subnet_id       = aws_subnet.client_two_pb_sn_az.id
  private_ips     = [cidrhost(cidrsubnet(aws_vpc.client_two_vpc.cidr_block,8, 1), 5)]
  security_groups = [aws_security_group.client_two.id]

  tags = merge(
    local.common_tags, {
      "Name" = "${var.project_name}-${local.client_two.region}-vm-a-eni"
  })
}

