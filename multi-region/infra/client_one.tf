#London
locals {
  client_one = {
    region        = "eu-west-2"
    vpc_cidr      = "10.158.0.0/16"
    ami_id        = "ami-0e2970aea15ce4984"
    instance_type = "t2.xlarge"
  }
}

#Create Amazon VPC with specific CIDR block
resource "aws_vpc" "client_one_vpc" {
  provider = aws.client_one

  cidr_block           = local.client_one.vpc_cidr
  enable_dns_hostnames = true

  tags = merge(
    local.common_tags, {
      "Name" = "${var.project_name}-${local.client_one.region}-${local.client_one.region}-${local.client_one.region}-vpc"
  })
}

#Create Amazon DHCP
resource "aws_vpc_dhcp_options" "client_one_dhcp" {
  provider = aws.client_one

  domain_name_servers = ["AmazonProvidedDNS"]

  tags = merge(
    local.common_tags, {
      "Name" = "${var.project_name}-${local.client_one.region}-${local.client_one.region}-${local.client_one.region}-dhcp"
  })
}

#Associate Amazon DHCP and Amazon VPC
resource "aws_vpc_dhcp_options_association" "client_one_vpc_dhcp" {
  provider = aws.client_one

  vpc_id          = aws_vpc.client_one_vpc.id
  dhcp_options_id = aws_vpc_dhcp_options.client_one_dhcp.id
}

#Create Amazon IGW
resource "aws_internet_gateway" "client_one_igw" {
  provider = aws.client_one

  vpc_id = aws_vpc.client_one_vpc.id

  tags = merge(
    local.common_tags, {
      "Name" = "${var.project_name}-${local.client_one.region}-${local.client_one.region}-${local.client_one.region}-igw"
  })
}

#Create `main` Amazon Route Table
resource "aws_route_table" "client_one_main" {
  provider = aws.client_one

  vpc_id = aws_vpc.client_one_vpc.id

  tags = merge(
    local.common_tags, {
      "Name" = "${var.project_name}-${local.client_one.region}-${local.client_one.region}-${local.client_one.region}-rt"
  })
}

#Create default route table rule
resource "aws_route" "client_one_default" {
  provider = aws.client_one

  route_table_id         = aws_route_table.client_one_main.id
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = aws_internet_gateway.client_one_igw.id
}

resource "aws_route" "client_one_to_replica_one" {
  provider = aws.client_one

  route_table_id            = aws_route_table.client_one_main.id
  destination_cidr_block    = local.replica_one.vpc_cidr
  vpc_peering_connection_id = aws_vpc_peering_connection.client_one_to_replica_one.id
}

#Create Amazon EC2 key pair
resource "aws_key_pair" "client_one_ec2_keypair" {
  provider = aws.client_one

  key_name   = "client-${var.project_name}-${local.client_one.region}-${local.client_one.region}-${local.client_one.region}-pub-key"
  public_key = var.ec2_keypair_public_key
}


#Create public subnet
resource "aws_subnet" "client_one_pb_sn_az" {
  provider = aws.client_one

  vpc_id            = aws_vpc.client_one_vpc.id
  cidr_block        = cidrsubnet(aws_vpc.client_one_vpc.cidr_block,8, 1)
  availability_zone = "${local.client_one.region}a"

  map_public_ip_on_launch = true

  tags = merge(
    local.common_tags, {
      "Name" = "${var.project_name}-${local.client_one.region}-${local.client_one.region}-${local.client_one.region}-pb-sn-a"
  })
}

#Associate public subnet with main route table
resource "aws_route_table_association" "client_one_pb_sn_az" {
  provider = aws.client_one

  subnet_id      = aws_subnet.client_one_pb_sn_az.id
  route_table_id = aws_route_table.client_one_main.id
}

#Configure VPC peering to replica_two
resource "aws_vpc_peering_connection" "client_one_to_replica_one" {
  provider = aws.client_one

  vpc_id      = aws_vpc.client_one_vpc.id
  peer_vpc_id = aws_vpc.replica_one_vpc.id
  peer_region = local.replica_one.region
  auto_accept = false

  tags = merge(
    local.common_tags,
    {
      "Name" = "client-to-one-peer"
    }
  )
}

resource "aws_vpc_peering_connection_accepter" "client_one_to_replica_one" {
  provider = aws.replica_one

  vpc_peering_connection_id = aws_vpc_peering_connection.client_one_to_replica_one.id
  auto_accept               = true

  tags = merge(
    local.common_tags,
    {
      "Name" = "client-to-one-peer"
    }
  )
}

resource "aws_security_group" "client_one" {
  provider    = aws.client_one
  name   = "${var.project_name}-${local.client_one.region}-${local.client_one.region}-vm-sg"
  vpc_id = aws_vpc.client_one_vpc.id

  tags = merge(
    local.common_tags, {
      "Name" = "${var.project_name}-${local.client_one.region}-${local.client_one.region}-vm-sg"
  })
}

resource "aws_security_group_rule" "inbound_cluster_to_client_one_vm" {
  provider    = aws.client_one
  security_group_id = aws_security_group.client_one.id
  description       = "Allow amy traffic"
  type              = "ingress"
  from_port         = "0"
  to_port           = "65535"
  protocol          = "tcp"
  cidr_blocks = [
    "0.0.0.0/0"
  ]
}

resource "aws_security_group_rule" "client_one_vm_outbound_any" {
  provider    = aws.client_one
  security_group_id = aws_security_group.client_one.id
  type              = "egress"
  from_port         = 0
  to_port           = 0
  protocol          = "all"
  cidr_blocks       = ["0.0.0.0/0"]
}


resource "aws_network_interface" "client_one_vm" {
  provider = aws.client_one

  subnet_id       = aws_subnet.client_one_pb_sn_az.id
  private_ips     = [cidrhost(cidrsubnet(aws_vpc.client_one_vpc.cidr_block,8, 1), 5)]
  security_groups = [aws_security_group.client_one.id]

  tags = merge(
    local.common_tags, {
      "Name" = "${var.project_name}-${local.client_one.region}-vm-a-eni"
  })
}

