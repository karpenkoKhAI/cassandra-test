#London
locals {
  replica_three = {
    region        = "ap-south-1"
    vpc_cidr      = "10.163.0.0/16"
    ami_id        = "ami-0ede599243e760d24"
    instance_type = "t2.xlarge"
  }
}

#Create Amazon VPC with specific CIDR block
resource "aws_vpc" "replica_three_vpc" {
  provider = aws.replica_three

  cidr_block           = local.replica_three.vpc_cidr
  enable_dns_hostnames = true

  tags = merge(
    local.common_tags, {
      "Name" = "${var.project_name}-${local.replica_three.region}-${local.replica_three.region}-${local.replica_three.region}-vpc"
  })
}

#Create Amazon DHCP
resource "aws_vpc_dhcp_options" "replica_three_dhcp" {
  provider = aws.replica_three

  domain_name_servers = ["AmazonProvidedDNS"]

  tags = merge(
    local.common_tags, {
      "Name" = "${var.project_name}-${local.replica_three.region}-${local.replica_three.region}-${local.replica_three.region}-dhcp"
  })
}

#Associate Amazon DHCP and Amazon VPC
resource "aws_vpc_dhcp_options_association" "replica_three_vpc_dhcp" {
  provider = aws.replica_three

  vpc_id          = aws_vpc.replica_three_vpc.id
  dhcp_options_id = aws_vpc_dhcp_options.replica_three_dhcp.id
}

#Create Amazon IGW
resource "aws_internet_gateway" "replica_three_igw" {
  provider = aws.replica_three

  vpc_id = aws_vpc.replica_three_vpc.id

  tags = merge(
    local.common_tags, {
      "Name" = "${var.project_name}-${local.replica_three.region}-${local.replica_three.region}-${local.replica_three.region}-igw"
  })
}

#Create `main` Amazon Route Table
resource "aws_route_table" "replica_three_main" {
  provider = aws.replica_three

  vpc_id = aws_vpc.replica_three_vpc.id

  tags = merge(
    local.common_tags, {
      "Name" = "${var.project_name}-${local.replica_three.region}-${local.replica_three.region}-${local.replica_three.region}-rt"
  })
}

#Create default route table rule
resource "aws_route" "replica_three_default" {
  provider = aws.replica_three

  route_table_id         = aws_route_table.replica_three_main.id
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = aws_internet_gateway.replica_three_igw.id
}

resource "aws_route" "replica_three_to_replica_two" {
  provider = aws.replica_three

  route_table_id            = aws_route_table.replica_three_main.id
  destination_cidr_block    = local.replica_two.vpc_cidr
  vpc_peering_connection_id = aws_vpc_peering_connection.replica_two_to_replica_three.id
}

resource "aws_route" "replica_three_to_replica_one" {
  provider = aws.replica_three

  route_table_id            = aws_route_table.replica_three_main.id
  destination_cidr_block    = local.replica_one.vpc_cidr
  vpc_peering_connection_id = aws_vpc_peering_connection.replica_one_to_replica_three.id
}

#Create Amazon EC2 key pair
resource "aws_key_pair" "replica_three_ec2_keypair" {
  provider = aws.replica_three

  key_name   = "${var.project_name}-${local.replica_three.region}-${local.replica_three.region}-${local.replica_three.region}-pub-key"
  public_key = var.ec2_keypair_public_key
}


#Create public subnet
resource "aws_subnet" "replica_three_pb_sn_az" {
  provider = aws.replica_three

  vpc_id            = aws_vpc.replica_three_vpc.id
  cidr_block        = cidrsubnet(aws_vpc.replica_three_vpc.cidr_block,8, 1)
  availability_zone = "${local.replica_three.region}a"

  map_public_ip_on_launch = true

  tags = merge(
    local.common_tags, {
      "Name" = "${var.project_name}-${local.replica_three.region}-${local.replica_three.region}-${local.replica_three.region}-pb-sn-a"
  })
}

#Associate public subnet with main route table
resource "aws_route_table_association" "replica_three_pb_sn_az" {
  provider = aws.replica_three

  subnet_id      = aws_subnet.replica_three_pb_sn_az.id
  route_table_id = aws_route_table.replica_three_main.id
}

resource "aws_security_group" "replica_three" {
  provider = aws.replica_three

  name   = "${var.project_name}-${local.replica_three.region}-${local.replica_three.region}-vm-sg"
  vpc_id = aws_vpc.replica_three_vpc.id

  tags = merge(
    local.common_tags, {
      "Name" = "${var.project_name}-${local.replica_three.region}-${local.replica_three.region}-vm-sg"
  })
}

resource "aws_security_group_rule" "inbound_cluster_to_replica_three_vm" {
  provider = aws.replica_three

  security_group_id = aws_security_group.replica_three.id
  description       = "Allow ssh protocol for any"
  type              = "ingress"
  from_port         = "0"
  to_port           = "65535"
  protocol          = "tcp"
  cidr_blocks = [
    "0.0.0.0/0"
  ]
}

resource "aws_security_group_rule" "replica_three_vm_outbound_any" {
  provider = aws.replica_three

  security_group_id = aws_security_group.replica_three.id
  type              = "egress"
  from_port         = 0
  to_port           = 0
  protocol          = "all"
  cidr_blocks       = ["0.0.0.0/0"]
}


resource "aws_network_interface" "replica_three_vm" {
  provider = aws.replica_three

  subnet_id       = aws_subnet.replica_three_pb_sn_az.id
  private_ips     = [cidrhost(cidrsubnet(aws_vpc.replica_three_vpc.cidr_block,8, 1), 5)]
  security_groups = [aws_security_group.replica_three.id]

  tags = merge(
    local.common_tags, {
      "Name" = "${var.project_name}-${local.replica_three.region}-vm-a-eni"
  })
}


