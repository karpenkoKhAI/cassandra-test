resource "local_file" "host" {
  content = templatefile("files/templates/hosts", {
    node_1 = aws_instance.replica_one_vm.public_ip
    node_2 = aws_instance.replica_two_vm.public_ip
    node_3 = aws_instance.replica_three_vm.public_ip
    client_1  = aws_instance.client_one_vm.public_ip
    client_2  = aws_instance.client_two_vm.public_ip
  })
  filename = "../os/inventory/hosts"
}

resource "local_file" "node_1" {
  content = templatefile("files/templates/node.yaml", {
    cluster_name   = "Amazon cassandra cluster"
    seeds          = join(",", [aws_instance.replica_one_vm.public_ip,aws_instance.replica_two_vm.public_ip,aws_instance.replica_three_vm.public_ip])
    listen_address = aws_instance.replica_one_vm.private_ip
    private_ip     = aws_instance.replica_one_vm.public_ip
  })
  filename = "../os/inventory/group_vars/node-1/conf.yaml"
}

resource "local_file" "node_2" {
  content = templatefile("files/templates/node.yaml", {
    cluster_name   = "Amazon cassandra cluster"
    seeds          = join(",", [aws_instance.replica_one_vm.public_ip,aws_instance.replica_two_vm.public_ip,aws_instance.replica_three_vm.public_ip])
    listen_address = aws_instance.replica_two_vm.private_ip
    private_ip     = aws_instance.replica_two_vm.public_ip
  })
  filename = "../os/inventory/group_vars/node-2/conf.yaml"
}

resource "local_file" "node_3" {
  content = templatefile("files/templates/node.yaml", {
    cluster_name   = "Amazon cassandra cluster"
    seeds          = join(",", [aws_instance.replica_one_vm.public_ip,aws_instance.replica_two_vm.public_ip,aws_instance.replica_three_vm.public_ip])
    listen_address = aws_instance.replica_three_vm.private_ip
    private_ip     = aws_instance.replica_three_vm.public_ip
  })
  filename = "../os/inventory/group_vars/node-3/conf.yaml"
}


resource "aws_instance" "client_one_vm" {
  provider = aws.client_one

  ami               = local.client_one.ami_id
  instance_type     = local.client_one.instance_type
  availability_zone = "${local.client_one.region}a"
  key_name          = aws_key_pair.client_one_ec2_keypair.key_name
  user_data = templatefile("./files/ec2/nix-userdata.tpl", {
    hostname = "client-one"
  })
  disable_api_termination = false

  network_interface {
    device_index         = 0
    network_interface_id = aws_network_interface.client_one_vm.id
  }

  root_block_device {
    volume_size           = "100"
    delete_on_termination = true
  }

  lifecycle {
    #[TEMP] Due to user-data forces new resource when you move terraform apply from windows to linux and vice versa
    ignore_changes = [user_data]
  }

  tags = merge(
    local.common_tags, {
      "Name" = "${var.project_name}-${local.client_one.region}-vm-a"
  })
}


resource "aws_instance" "client_two_vm" {
  provider = aws.client_two

  ami               = local.client_two.ami_id
  instance_type     = local.client_two.instance_type
  availability_zone = "${local.client_two.region}a"
  key_name          = aws_key_pair.client_two_ec2_keypair.key_name
  user_data = templatefile("./files/ec2/nix-userdata.tpl", {
    hostname = "client-two"
  })
  disable_api_termination = false

  network_interface {
    device_index         = 0
    network_interface_id = aws_network_interface.client_two_vm.id
  }

  root_block_device {
    volume_size           = "100"
    delete_on_termination = true
  }

  lifecycle {
    #[TEMP] Due to user-data forces new resource when you move terraform apply from windows to linux and vice versa
    ignore_changes = [user_data]
  }

  tags = merge(
    local.common_tags, {
      "Name" = "${var.project_name}-${local.client_two.region}-vm-a"
  })
}

resource "aws_instance" "replica_one_vm" {
  provider = aws.replica_one

  ami               = local.replica_one.ami_id
  instance_type     = local.replica_one.instance_type
  availability_zone = "${local.replica_one.region}a"
  key_name          = aws_key_pair.replica_one_ec2_keypair.key_name
  user_data = templatefile("./files/ec2/nix-userdata.tpl", {
    hostname = "replica-one"
  })
  disable_api_termination = false

  network_interface {
    device_index         = 0
    network_interface_id = aws_network_interface.replica_one_vm.id
  }

  root_block_device {
    volume_size           = "100"
    delete_on_termination = true
  }

  lifecycle {
    #[TEMP] Due to user-data forces new resource when you move terraform apply from windows to linux and vice versa
    ignore_changes = [user_data]
  }

  tags = merge(
    local.common_tags, {
      "Name" = "${var.project_name}-${local.replica_one.region}-vm-a"
  })
}

resource "aws_instance" "replica_three_vm" {
  provider = aws.replica_three

  ami               = local.replica_three.ami_id
  instance_type     = local.replica_three.instance_type
  availability_zone = "${local.replica_three.region}a"
  key_name          = aws_key_pair.replica_three_ec2_keypair.key_name
  user_data = templatefile("./files/ec2/nix-userdata.tpl", {
    hostname = "replica-three"
  })
  disable_api_termination = false

  network_interface {
    device_index         = 0
    network_interface_id = aws_network_interface.replica_three_vm.id
  }

  root_block_device {
    volume_size           = "100"
    delete_on_termination = true
  }

  lifecycle {
    #[TEMP] Due to user-data forces new resource when you move terraform apply from windows to linux and vice versa
    ignore_changes = [user_data]
  }

  tags = merge(
    local.common_tags, {
      "Name" = "${var.project_name}-${local.replica_three.region}-vm-a"
  })
}

resource "aws_instance" "replica_two_vm" {
  provider = aws.replica_two

  ami               = local.replica_two.ami_id
  instance_type     = local.replica_two.instance_type
  availability_zone = "${local.replica_two.region}a"
  key_name          = aws_key_pair.replica_two_ec2_keypair.key_name
  user_data = templatefile("./files/ec2/nix-userdata.tpl", {
    hostname = "replica-two"
  })
  disable_api_termination = false

  network_interface {
    device_index         = 0
    network_interface_id = aws_network_interface.replica_two_vm.id
  }

  root_block_device {
    volume_size           = "100"
    delete_on_termination = true
  }

  lifecycle {
    #[TEMP] Due to user-data forces new resource when you move terraform apply from windows to linux and vice versa
    ignore_changes = [user_data]
  }

  tags = merge(
    local.common_tags, {
      "Name" = "${var.project_name}-${local.replica_two.region}-vm-a"
  })
}

output "client_one" {
  value = {
    private_ip = aws_instance.client_one_vm.private_ip
    public_ip  = aws_instance.client_one_vm.public_ip
  }
}

output "client_two" {
  value = {
    private_ip = aws_instance.client_two_vm.private_ip
    public_ip  = aws_instance.client_two_vm.public_ip
  }
}

output "replica_one" {
  value = {
    private_ip = aws_instance.replica_one_vm.private_ip
    public_ip  = aws_instance.replica_one_vm.public_ip
  }
}

output "replica_two" {
  value = {
    private_ip = aws_instance.replica_two_vm.private_ip
    public_ip  = aws_instance.replica_two_vm.public_ip
  }
}

output "replica_three" {
  value = {
    private_ip = aws_instance.replica_three_vm.private_ip
    public_ip  = aws_instance.replica_three_vm.public_ip
  }
}