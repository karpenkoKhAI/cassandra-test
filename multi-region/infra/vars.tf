variable "project_name" {
  default = "karpenko-test"
}
# variable "vm" {
#   default = [
#     {
#       ami_id        = "ami-021b01acbbe9b4e73"
#       instance_type = "t2.xlarge"
#     },
#     {
#       ami_id        = "ami-021b01acbbe9b4e73"
#       instance_type = "t2.xlarge"
#     },
#     {
#       ami_id        = "ami-021b01acbbe9b4e73"
#       instance_type = "t2.xlarge"
#     }
#     # {
#     #   ami_id        = "ami-021b01acbbe9b4e73"
#     #   instance_type = "t2.xlarge"
#     # },
#     # {
#     #   ami_id        = "ami-021b01acbbe9b4e73"
#     #   instance_type = "t2.xlarge"
#     # },
#     # {
#     #   ami_id        = "ami-021b01acbbe9b4e73"
#     #   instance_type = "t2.xlarge"
#     # },
#     # {
#     #   ami_id        = "ami-021b01acbbe9b4e73"
#     #   instance_type = "t2.xlarge"
#     # }
#   ]
# }

# variable "cm" {
#   default = {
#     ami_id        = "ami-021b01acbbe9b4e73"
#     instance_type = "t2.xlarge"
#   }
# }

variable "ec2_keypair_public_key" {
  default = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCQ+mEXKAMUMSsdTGGdEubzBIpMkzErn51XqC1vTKZqVJojae2mwvPCD5baHYqZ7jV5fWjM4OirHhrhvVTH4mivPzM+G2ibjHUmx6eGw2/n12OrMJyXxtVup67Jo9lxlXGDnHFytUTEi+deCHYghtftjs5CZaiKlVf2G7p8ja1fe0FjWRmIIe7MmYjwK4Zr9goH42HBNc1JsroV1/qcFdvOh6eDFq64xJdqHpja2fDIa7nhGglybC35G5Yn3EQ71CWVGF0zTXZ2JxfvgJ4pLQ5C9apkbA4rem3M+/xsfrVKbLAd2LAr7b62fUlPEB2uGsLJPFKWpwRJXYEI0WDfL5gf"
}

locals {
  common_tags = {
    "Owner"   = "andrii.karpenko@stryker.com"
    "Project" = "Training"
    "Purpose" = "Training"
    "Removal" = "24/12/2021"
  }
}