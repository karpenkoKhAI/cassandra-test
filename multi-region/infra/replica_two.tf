#London
locals {
  replica_two = {
    region        = "ca-central-1"
    vpc_cidr      = "10.162.0.0/16"
    ami_id        = "ami-0f33e328c2dd2d6b0"
    instance_type = "t2.xlarge"
  }
}

#Create Amazon VPC with specific CIDR block
resource "aws_vpc" "replica_two_vpc" {
  provider = aws.replica_two

  cidr_block           = local.replica_two.vpc_cidr
  enable_dns_hostnames = true

  tags = merge(
    local.common_tags, {
      "Name" = "${var.project_name}-${local.replica_two.region}-${local.replica_two.region}-${local.replica_two.region}-vpc"
  })
}

#Create Amazon DHCP
resource "aws_vpc_dhcp_options" "replica_two_dhcp" {
  provider = aws.replica_two

  domain_name_servers = ["AmazonProvidedDNS"]

  tags = merge(
    local.common_tags, {
      "Name" = "${var.project_name}-${local.replica_two.region}-${local.replica_two.region}-${local.replica_two.region}-dhcp"
  })
}

#Associate Amazon DHCP and Amazon VPC
resource "aws_vpc_dhcp_options_association" "replica_two_vpc_dhcp" {
  provider = aws.replica_two

  vpc_id          = aws_vpc.replica_two_vpc.id
  dhcp_options_id = aws_vpc_dhcp_options.replica_two_dhcp.id
}

#Create Amazon IGW
resource "aws_internet_gateway" "replica_two_igw" {
  provider = aws.replica_two

  vpc_id = aws_vpc.replica_two_vpc.id

  tags = merge(
    local.common_tags, {
      "Name" = "${var.project_name}-${local.replica_two.region}-${local.replica_two.region}-${local.replica_two.region}-igw"
  })
}

#Create `main` Amazon Route Table
resource "aws_route_table" "replica_two_main" {
  provider = aws.replica_two

  vpc_id = aws_vpc.replica_two_vpc.id

  tags = merge(
    local.common_tags, {
      "Name" = "${var.project_name}-${local.replica_two.region}-${local.replica_two.region}-${local.replica_two.region}-rt"
  })
}

#Create default route table rule
resource "aws_route" "replica_two_default" {
  provider = aws.replica_two

  route_table_id         = aws_route_table.replica_two_main.id
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = aws_internet_gateway.replica_two_igw.id
}

resource "aws_route" "replica_two_to_replica_one" {
  provider = aws.replica_two

  route_table_id            = aws_route_table.replica_two_main.id
  destination_cidr_block    = local.replica_one.vpc_cidr
  vpc_peering_connection_id = aws_vpc_peering_connection.replica_one_to_replica_two.id
}

resource "aws_route" "replica_two_to_replica_three" {
  provider = aws.replica_two

  route_table_id            = aws_route_table.replica_two_main.id
  destination_cidr_block    = local.replica_three.vpc_cidr
  vpc_peering_connection_id = aws_vpc_peering_connection.replica_two_to_replica_three.id
}

#Create Amazon EC2 key pair
resource "aws_key_pair" "replica_two_ec2_keypair" {
  provider = aws.replica_two

  key_name   = "${var.project_name}-${local.replica_two.region}-${local.replica_two.region}-${local.replica_two.region}-pub-key"
  public_key = var.ec2_keypair_public_key
}


#Create public subnet
resource "aws_subnet" "replica_two_pb_sn_az" {
  provider = aws.replica_two

  vpc_id            = aws_vpc.replica_two_vpc.id
  cidr_block        = cidrsubnet(aws_vpc.replica_two_vpc.cidr_block,8, 1)
  availability_zone = "${local.replica_two.region}a"

  map_public_ip_on_launch = true

  tags = merge(
    local.common_tags, {
      "Name" = "${var.project_name}-${local.replica_two.region}-${local.replica_two.region}-${local.replica_two.region}-pb-sn-a"
  })
}

#Associate public subnet with main route table
resource "aws_route_table_association" "replica_two_pb_sn_az" {
  provider = aws.replica_two

  subnet_id      = aws_subnet.replica_two_pb_sn_az.id
  route_table_id = aws_route_table.replica_two_main.id
}

#Configure VPC peering to replica_three
resource "aws_vpc_peering_connection" "replica_two_to_replica_three" {
  provider    = aws.replica_two

  vpc_id      = aws_vpc.replica_two_vpc.id
  peer_vpc_id = aws_vpc.replica_three_vpc.id
  peer_region = local.replica_three.region
  auto_accept = false

  tags = merge(
    local.common_tags,
    {
      "Name" = "two-to-three-peer"
    }
  )
}

resource "aws_vpc_peering_connection_accepter" "replica_two_to_replica_three" {
  provider = aws.replica_three

  vpc_peering_connection_id = aws_vpc_peering_connection.replica_two_to_replica_three.id
  auto_accept               = true

  tags = merge(
    local.common_tags,
    {
      "Name" = "two-to-three-peer"
    }
  )
}

resource "aws_security_group" "replica_two" {
  provider    = aws.replica_two

  name   = "${var.project_name}-${local.replica_two.region}-${local.replica_two.region}-vm-sg"
  vpc_id = aws_vpc.replica_two_vpc.id

  tags = merge(
    local.common_tags, {
      "Name" = "${var.project_name}-${local.replica_two.region}-${local.replica_two.region}-vm-sg"
  })
}

resource "aws_security_group_rule" "inbound_cluster_to_replica_two_vm" {
  provider    = aws.replica_two

  security_group_id = aws_security_group.replica_two.id
  description       = "Allow ssh protocol for any"
  type              = "ingress"
  from_port         = "0"
  to_port           = "65535"
  protocol          = "tcp"
  cidr_blocks = [
    "0.0.0.0/0"
  ]
}

resource "aws_security_group_rule" "replica_two_vm_outbound_any" {
  provider    = aws.replica_two

  security_group_id = aws_security_group.replica_two.id
  type              = "egress"
  from_port         = 0
  to_port           = 0
  protocol          = "all"
  cidr_blocks       = ["0.0.0.0/0"]
}


resource "aws_network_interface" "replica_two_vm" {
  provider = aws.replica_two

  subnet_id       = aws_subnet.replica_two_pb_sn_az.id
  private_ips     = [cidrhost(cidrsubnet(aws_vpc.replica_two_vpc.cidr_block,8, 1), 5)]
  security_groups = [aws_security_group.replica_two.id]

  tags = merge(
    local.common_tags, {
      "Name" = "${var.project_name}-${local.replica_two.region}-vm-a-eni"
  })
}

