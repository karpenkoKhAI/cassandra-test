import csv

from prettytable import PrettyTable
from scipy.stats import variation
import math


def calc_params(filename, threads, operation):
    "This function calculate parameters"
    tmp = []
    latency_values = []
    avr_latency = 0
    length = 0
    delta = 0
    time = 0
    items = 0
    coefficient = 0.1
    with open(filename, 'r') as data:
        content = csv.DictReader(data)
        tmp = list(content)
        if operation == 'read':
            length = len(tmp) - (threads + 2)
            delta = int((length * coefficient) / 2)
            for i in range(delta + int((threads/100)*5000), (length - delta)):
                avr_latency += int(tmp[i][' latency(us)'])
                latency_values.append(int(tmp[i][' latency(us)']))
                items = items + 1
            time = float(tmp[(len(tmp) - delta)][' timestamp(ms)']
                         ) - float(tmp[delta][' timestamp(ms)'])
        else:
            length = len(tmp) - (threads + 2)
            delta = int((length * coefficient) / 2)
            for i in range((delta + int((threads/100)*5000) + (threads + 2)),  (length - delta)):
                avr_latency += int(tmp[i][' latency(us)'])
                latency_values.append(int(tmp[i][' latency(us)']))
                items = items + 1
            time = float(tmp[(len(tmp) - delta)][' timestamp(ms)']
                         ) - float(tmp[delta][' timestamp(ms)'])
        return items, avr_latency/items, variation(latency_values), items/(time * 0.001)


filePath = "/Users/andrej/Desktop/client31"
read_latency = PrettyTable()
# read_latency.field_names = ["Threads", "ONE latency", "QUORUM latency",
#                             "ALL latency", "ONE variation", "QUORUM variation", "ALL variation"]
read_latency.field_names = ["Threads",
                            "ONE ops/s", "QUORUM ops/s", "ALL ops/s"]

##############READ###############
for i in [100, 200, 300, 400, 500, 600, 700, 800, 900, 1000]:
    one_count, one_latency, one_variation, one_ops = calc_params(
        "%s/%s_%s_%s.csv" % (filePath, i, "ONE", "read"), i, "read")
    quorum_count, quorum_latency, quorum_variation, quorum_ops = calc_params(
        "%s/%s_%s_%s.csv" % (filePath, i, "QUORUM", "read"), i, "read")
    all_count, all_latency, all_variation, all_ops = calc_params(
        "%s/%s_%s_%s.csv" % (filePath, i, "ALL", "read"), i, "read")
    # read_latency.add_row([i, one_latency, quorum_latency, all_latency,
    #                      one_variation, quorum_variation, all_variation])
    read_latency.add_row([i, one_ops, quorum_ops, all_ops])

print(read_latency)

write_latency = PrettyTable()
# write_latency.field_names = ["Threads", "ONE latency", "QUORUM latency",
#                              "ALL latency", "ONE variation", "QUORUM variation", "ALL variation"]
write_latency.field_names = ["Threads",


                             "ONE ops/s", "QUORUM ops/s", "ALL ops/s"]

##############WRITE##############
for i in [100, 200, 300, 400, 500, 600, 700, 800, 900, 1000]:
    one_count, one_latency, one_variation, one_ops = calc_params(
        "%s/%s_%s_%s.csv" % (filePath, i, "ONE", "write"), i, "write")
    quorum_count, quorum_latency, quorum_variation, quorum_ops = calc_params(
        "%s/%s_%s_%s.csv" % (filePath, i, "QUORUM", "write"), i, "write")
    all_count, all_latency, all_variation, all_ops = calc_params(
        "%s/%s_%s_%s.csv" % (filePath, i, "ALL", "write"), i, "write")
    # write_latency.add_row([i, one_latency, quorum_latency, all_latency,
    #                       one_variation, quorum_variation, all_variation])
    write_latency.add_row([i, one_ops, quorum_ops, all_ops])


print(write_latency)
