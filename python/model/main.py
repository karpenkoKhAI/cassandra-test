from numpy.random import default_rng
import numpy as np
import random
import matplotlib.pyplot as plt


def model_behavior(replicasCount=3, distributionName="gamma", distributionShape=10000, distributionScale=6, consistencyLevel="one", speculativeRetry="false", speculativeRetryCalls=0, minDelay=0):
    delays = []
    resultDelay = 0
    rng = default_rng()

    if distributionName == "gamma":
        delays = rng.gamma(distributionShape, distributionScale, replicasCount)
    elif distributionName == "weibull":
        delays = rng.weibull(distributionShape, replicasCount)
    else:
        print(f"Entered distribution [{distributionName}] does not support")
        raise ValueError(
            "The distribution should be one of [gamma or weibull]")

    # # Sort delays
    # indexes = sorted(range(len(delays)), key=lambda i: delays[i])

    # print(f"Sorted delays by replicas {indexes}")

    # print(f"Generated delays using {distributionName}: {delays}")

    # if speculativeRetry == "true" or speculativeRetry == "True":
    #     for i in range(0, replicasCount - 1):
    #         print(f"[{i}] The delay is {delays[indexes[i]]} in replica {indexes[i]}")
    # else:
    #     print(f"The min delay is {delays[indexes[0]]} in replica {indexes[0]}")

    if consistencyLevel == "one" or consistencyLevel == "One":
        if speculativeRetry == "false" and speculativeRetryCalls == 1:
            # 1 - 1 - 3: get random value from array of delays
            resultDelay = delays[random.randint(0, replicasCount - 1)]
            print(f"1-1-3:{resultDelay}")
        elif speculativeRetry == "true" and (speculativeRetryCalls > 1 and speculativeRetryCalls < replicasCount):
            # delays = delays.sort() # 1 - 2 - 3 and 1 - 3 - 3: get min delay from array of delays
            tmp = []
            j = 0
            for i in range(0, int(replicasCount/2)+1):
                j = random.randint(0, replicasCount - 1)  # random index
                while 1:
                    if delays[j] in tmp:
                        j = random.randint(
                            0, replicasCount - 1)  # random index
                    else:
                        break
                tmp.append(delays[j])
            sortedDelay = sorted(tmp)
            # print("Delays:")
            # print(delays)
            # print("Sorted:")
            # print(sortedDelay)
            resultDelay = sortedDelay[0]
            # print("Result:")
            # print(resultDelay)
            print(f"1-2-3:{resultDelay}")
        elif speculativeRetry == "true" and (speculativeRetryCalls > 1 and speculativeRetryCalls == replicasCount):
            sortedDelay = sorted(delays)
            resultDelay = sortedDelay[0]
            print(f"1-2-3:{resultDelay}")
        else:
            print(f"1-2(3)-3:error")
            resultDelay = "error"  # Not valid params
    elif consistencyLevel == "quorum" or consistencyLevel == "Quorum":
        if speculativeRetry == "false" and (int(replicasCount/2)+1) == speculativeRetryCalls:
            tmp = []
            j = 0
            for i in range(0, int(replicasCount/2)+1):
                j = random.randint(0, replicasCount - 1)  # random index
                while 1:
                    if delays[j] in tmp:
                        j = random.randint(
                            0, replicasCount - 1)  # random index
                    else:
                        break
                tmp.append(delays[j])
            # 2 - 2 - 3:
            sortedDelay = sorted(tmp)
            resultDelay = sortedDelay[(int(replicasCount/2)+1)-1]
            print(f"2-2-3:{resultDelay}")
        elif speculativeRetry == "true" and (speculativeRetryCalls > (int(replicasCount/2)+1) and speculativeRetryCalls <= replicasCount):
            # 2 - 3 - 3: get min delay from array of delays
            sortedDelay = sorted(delays)
            resultDelay = sortedDelay[(int(replicasCount/2)+1)-1]
            print(f"2-3-3:{resultDelay}")
        else:
            print(f"2-2(3)-3:error")
            resultDelay = "error"  # Not valid params
    else:
        # 3 - 3 - 3: get max value form array of delays
        sortedDelay = sorted(delays)
        resultDelay = sortedDelay[replicasCount - 1]
        print(f"3-3-3:{resultDelay}")

    return resultDelay + minDelay, delays


shape_100 = 1.97
scale_100 = 1014.40
minDelay_100 = 619

scale_200 = 2.41
shape_200 = 1516.77
minDelay_200 = 1759

scale_300 = 10.33
shape_300 = 219.38
minDelay_300 = 3751

# scale_400 = 14
# shape_400 = 734

# scale_600 = 17
# shape_600 = 865

# scale_800 = 20
# shape_800 = 946

# scale_900 = 24
# shape_900 = 1079

# scale_1200 = 27
# shape_1200 = 1185


##### 100, 200, 300
# 1-1-3 100 req/s shape = 2.554678, scale = 1126.388333
temp_1_1_3_100 = []
res_delays = []
for i in range(0, 1000):
    res, delays = model_behavior(3, "gamma", shape_100,
                                         scale_100, "one", "false", 1,minDelay_100)
    res_delays.append(delays)
    temp_1_1_3_100.append(res)

np.savetxt("1_1_3_100.csv", temp_1_1_3_100, delimiter=',')
np.savetxt("1_1_3_100_delays.csv", res_delays, delimiter=',')

temp_1_1_3_200 = []
res_delays = []
for i in range(0, 1000):
    res, delays = model_behavior(3, "gamma", shape_200,
                                         scale_200, "one", "false", 1,minDelay_200)
    res_delays.append(delays)
    temp_1_1_3_200.append(res)

np.savetxt("1_1_3_200.csv", temp_1_1_3_200, delimiter=',')
np.savetxt("1_1_3_200_delays.csv", res_delays, delimiter=',')


temp_1_1_3_300 = []
res_delays = []
for i in range(0, 1000):
    res, delays = model_behavior(3, "gamma", shape_300,
                                         scale_300, "one", "false", 1,minDelay_300)
    res_delays.append(delays)
    temp_1_1_3_300.append(res)

np.savetxt("1_1_3_300.csv", temp_1_1_3_300, delimiter=',')
np.savetxt("1_1_3_300_delays.csv", res_delays, delimiter=',')

# 1-2-3 200 req/s shape = 2.255489, scale = 2847.010385
temp_1_2_3_200 = []
res_delays = []
for i in range(0, 1000):
    res, delays = model_behavior(3, "gamma", shape_200,
                                         scale_200, "one", "true", 2,minDelay_200)
    res_delays.append(delays)
    temp_1_2_3_200.append(res)

np.savetxt("1_2_3_200.csv", temp_1_2_3_200, delimiter=',')
np.savetxt("1_2_3_200_delays.csv", res_delays, delimiter=',')

# 1-3-3 300 req/s shape =  0.713627, scale 12075.153294
temp_1_3_3_300 = []
res_delays = []
for i in range(0, 1000):
    res, delays = model_behavior(3, "gamma", shape_300,
                                         scale_300, "one", "true", 3, minDelay_300)
    res_delays.append(delays)
    temp_1_3_3_300.append(res)

np.savetxt("1_3_3_300.csv", temp_1_3_3_300, delimiter=',')
np.savetxt("1_3_3_300_delays.csv", res_delays, delimiter=',')

# 2-2-3 200 req/s shape = 2.255489, scale = 2847.010385
temp_2_2_3_200 = []
res_delays = []
for i in range(0, 5):
    res, delays = model_behavior(3, "gamma", shape_200,
                                         scale_200, "quorum", "false", 2,minDelay_200)
    res_delays.append(delays)
    temp_2_2_3_200.append(res)

np.savetxt("2_2_3_200.csv", temp_2_2_3_200, delimiter=',')
np.savetxt("2_2_3_200_delays.csv", res_delays, delimiter=',')


# 2-3-3 300 req/s shape =  0.713627, scale = 12075.153294
temp_2_3_3_300 = []
res_delays = []
for i in range(0, 1000):
    res, delays = model_behavior(3, "gamma", shape_300,
                                         scale_300, "quorum", "true", 3,minDelay_300)
    res_delays.append(delays)
    temp_2_3_3_300.append(res)

np.savetxt("2_3_3_300.csv", temp_2_3_3_300, delimiter=',')
np.savetxt("2_3_3_300_delays.csv", res_delays, delimiter=',')

# 3-3-3 300 req/s shape =  0.713627, scale = 12075.153294
temp_3_3_3_300 = []
res_delays = []
for i in range(0, 5):
    res, delays = model_behavior(3, "gamma", shape_300,
                                         scale_300, "all", "false", 3,minDelay_300)
    res_delays.append(delays)
    temp_3_3_3_300.append(res)

np.savetxt("3_3_3_300.csv", temp_3_3_3_300, delimiter=',')
np.savetxt("3_3_3_300_delays.csv", res_delays, delimiter=',')

# plt.hist([temp_1_1_3_100, temp_1_1_3_200, temp_1_1_3_300], alpha=0.5, bins=np.arange(
#     1000, 8000, 250), edgecolor='black', label=["1_1_3_100", "1_1_3_200", "1_1_3_300"])


# plt.hist([temp_1_1_3_100, temp_1_2_3_200, temp_1_3_3_300, temp_2_2_3_200, temp_2_3_3_300, temp_3_3_3_300], alpha=0.5, bins=np.arange(
#     1000, 50000, 1000), edgecolor='black', label=["1_1_3_100", "1_2_3_200", "1_3_3_300", "2_2_3_200", "2_3_3_300", "3_3_3_300"])

# plt.hist([temp_1_1_3_100, temp_1_2_3_200, temp_1_3_3_300, temp_2_2_3_200, temp_2_3_3_300, temp_3_3_3_300], alpha=0.5, bins=np.arange(
#     1000, 8000, 100), edgecolor='black', label=["1_1_3_100", "1_2_3_200", "1_3_3_300", "2_2_3_200", "2_3_3_300", "3_3_3_300"])

# plt.hist([temp_1_1_3_300, temp_1_2_3_600, temp_1_3_3_900, temp_2_2_3_600, temp_2_3_3_900, temp_3_3_3_900], alpha=0.5, bins=np.arange(
#     1000, 8000, 100), edgecolor='black', label=["1_1_3_300", "1_2_3_600", "1_3_3_900", "2_2_3_600", "2_3_3_900", "3_3_3_900"])

plt.hist(temp_1_1_3_100,alpha=0.5,bins=np.arange(1000, 8000, 100),edgecolor='black',label="1_1_3_100")
plt.hist(temp_1_2_3_200,alpha=0.5,bins=np.arange(1000, 8000, 100),edgecolor='black',label="1_2_3_200")
plt.hist(temp_1_3_3_300,alpha=0.5,bins=np.arange(1000, 8000, 100),edgecolor='black',label="1_3_3_300")
plt.hist(temp_2_2_3_200,alpha=0.5,bins=np.arange(1000, 8000, 100),edgecolor='black',label="2_2_3_200")
plt.hist(temp_2_3_3_300,alpha=0.5,bins=np.arange(1000, 8000, 100),edgecolor='black',label="2_3_3_300")
plt.hist(temp_3_3_3_300,alpha=0.5,bins=np.arange(1000, 8000, 100),edgecolor='black',label="3_3_3_300")

# plt.hist(temp_1_1_3_200,alpha=0.5,bins=np.arange(1000, 8000, 100),edgecolor='black',label="1_1_3_200")
# plt.hist(temp_1_2_3_400,alpha=0.5,bins=np.arange(1000, 8000, 100),edgecolor='black',label="1_2_3_400")
# plt.hist(temp_1_3_3_600,alpha=0.5,bins=np.arange(1000, 8000, 100),edgecolor='black',label="1_3_3_600")
# plt.hist(temp_2_2_3_400,alpha=0.5,bins=np.arange(1000, 8000, 100),edgecolor='black',label="2_2_3_400")
# plt.hist(temp_2_3_3_600,alpha=0.5,bins=np.arange(1000, 8000, 100),edgecolor='black',label="2_3_3_600")
# plt.hist(temp_3_3_3_600,alpha=0.5,bins=np.arange(1000, 8000, 100),edgecolor='black',label="3_3_3_600")

# plt.hist(temp_1_1_3_300,alpha=0.5,bins=np.arange(1000, 8000, 100),edgecolor='black',label="1_1_3_300")
# plt.hist(temp_1_2_3_600,alpha=0.5,bins=np.arange(1000, 8000, 100),edgecolor='black',label="1_2_3_600")
# plt.hist(temp_1_3_3_900,alpha=0.5,bins=np.arange(1000, 8000, 100),edgecolor='black',label="1_3_3_900")
# plt.hist(temp_2_2_3_600,alpha=0.5,bins=np.arange(1000, 8000, 100),edgecolor='black',label="2_2_3_600")
# plt.hist(temp_2_3_3_900,alpha=0.5,bins=np.arange(1000, 8000, 100),edgecolor='black',label="2_3_3_900")
# plt.hist(temp_3_3_3_900,alpha=0.5,bins=np.arange(1000, 8000, 100),edgecolor='black',label="3_3_3_900")

# plt.hist(temp_1_1_3_400,alpha=0.5,bins=np.arange(1000, 8000, 100),edgecolor='black',label="1_1_3_400")
# plt.hist(temp_1_2_3_800,alpha=0.5,bins=np.arange(1000, 8000, 100),edgecolor='black',label="1_2_3_800")
# plt.hist(temp_1_3_3_1200,alpha=0.5,bins=np.arange(1000, 8000, 100),edgecolor='black',label="1_3_3_1200")
# plt.hist(temp_2_2_3_800,alpha=0.5,bins=np.arange(1000, 8000, 100),edgecolor='black',label="2_2_3_800")
# plt.hist(temp_2_3_3_1200,alpha=0.5,bins=np.arange(1000, 8000, 100),edgecolor='black',label="2_3_3_1200")
# plt.hist(temp_3_3_3_1200,alpha=0.5,bins=np.arange(1000, 8000, 100),edgecolor='black',label="3_3_3_1200")
plt.xlabel('Count')
plt.ylabel('Delay')
plt.legend(loc='upper right')
plt.show()
